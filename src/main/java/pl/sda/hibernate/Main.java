package pl.sda.hibernate;


import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;

public class Main {
    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        final MongoDatabase sdaDb = client.getDatabase("sda");
        final MongoCollection<Document> documents = sdaDb.getCollection("documents");
        Document doc = new Document("_id", new ObjectId())
                .append("klucz", "wartosc");
        documents.insertOne(doc);
    }
}
